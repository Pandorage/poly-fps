﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float sensivity = 100f;

    public Transform playerBody;

    private float _xRotation = 0f;

    void Start()
    {
        //Lock the cursor to avoid player from clicking out of the screen
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        //Get the mouse movement
        float mouseX = Input.GetAxis("Mouse X") * sensivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensivity * Time.deltaTime;

        //Change the Camera rotation according to the mouse movement
        _xRotation -= mouseY;
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);
        if (_xRotation > 75)
        {
            _xRotation = 75;
        } else if (_xRotation < -75)
        {
            _xRotation = -75;
        }
        
        //Rotate the player transform to follow the camera rotation
        transform.localRotation = Quaternion.Euler(_xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
