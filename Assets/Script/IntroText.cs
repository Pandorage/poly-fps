﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroText : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Hide", 2f);
    }

    private void Hide()
    {
        Destroy(gameObject);
    }
}
