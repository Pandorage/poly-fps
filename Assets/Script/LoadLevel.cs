﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    private float _targetNumber;

    private int _sceneNumber;

    private float _timer = 5;
    
    public TMP_Text showCountdown;
    
    // Update is called once per frame
    void Update()
    {
        _targetNumber = GameObject.FindGameObjectsWithTag("target").Length; // find number of gameobject with target tag
        _sceneNumber = SceneManager.GetActiveScene().buildIndex; // find actual scene number
        
        if (_targetNumber <= 0)
        {
            gameObject.GetComponent<TargetCountdown>().enabled = false; // find and disenabled the script
            int timerUI = Mathf.FloorToInt(_timer);
            showCountdown.text = "Timer : " + timerUI; // show timer in a textmeshpro textbox
            _timer -= Time.deltaTime; // timer 
            
        }

        if (_timer <= 0)
        {
            if (SceneManager.GetActiveScene().buildIndex == 3)
            {
                SceneManager.LoadScene(0); // go to the menu
            }
            else
            {
                SceneManager.LoadScene(_sceneNumber+1); // go to the next scene from actual scene
            }
        }
        
    }
}
