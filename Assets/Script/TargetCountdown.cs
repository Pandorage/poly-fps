﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TargetCountdown : MonoBehaviour
{
    private float _targetNumber;

    public TMP_Text showTargetNumber;

    // Update is called once per frame
    void Update()
    {
        _targetNumber = GameObject.FindGameObjectsWithTag("target").Length; // find number of gameobject with target tag
        showTargetNumber.text = "Target :" + _targetNumber.ToString(); // show this number in a textmeshpro textbox
    }
}
