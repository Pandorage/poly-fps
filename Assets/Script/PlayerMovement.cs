﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed;
    public float gravity = -9.81f;
    public float checkDistance = 0.3f;
    public Transform groundCheck;
    public LayerMask groundMask;
    public float jumpHeight;
    
    private bool _isGrounded = false;
    private Vector3 _velocity;
    
    void Update()
    {
        _isGrounded = Physics.CheckSphere(groundCheck.position, checkDistance, groundMask); // check if the player is on the ground

        if (_isGrounded && _velocity.y < 0) 
        {
            _velocity.y = -2f;  // if the player is one the ground change his velocity for y axe
        }
        
        
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.forward * z + transform.right * x; // create a vector for the movement on z and x axes
        controller.Move(move * speed * Time.deltaTime);  // movement on z and x axes

        
        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);  // make the player jump
        }
        
        _velocity.y += gravity * Time.deltaTime;
        controller.Move(_velocity * Time.deltaTime); // create the gravity for the player
    }
}
