﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Secret : MonoBehaviour
{
    public GunShot gun;
    public Animator water;
    public OpenChest chest;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("secret"))
        {
            gun.secret = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("secret"))
        { 
            gun.secret = false;
        }
    }

    public void UnlockSecret()
    {
        water.SetTrigger("Fall");
        chest.unlocked = true;
    }
}
