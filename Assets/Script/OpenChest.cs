﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenChest : MonoBehaviour
{
    public Animator chestTop;
    public bool unlocked;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject.CompareTag("Player") && unlocked)
        {
            chestTop.SetTrigger("Chest");
        }
    }
}
