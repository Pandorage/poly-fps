﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunShot : MonoBehaviour
{
    public float range = 100f;
    public Camera cam;
    public ParticleSystem shootParticle;
    public Animator anim;
    public float shotCooldown;
    private float _nextShot;
    public bool secret;

    void Update()
    {
        //Cooldown and input verification
        if (Input.GetButton("Fire1") && Time.time >= _nextShot)
        {
            //Set the next shot timer
            _nextShot = Time.time + shotCooldown;
            Shoot();
        }
    }

    void Shoot()
    {
        //Play particle effect and weapon animation
        shootParticle.Play();
        anim.SetTrigger("Shot");
        
        //Lauch a raycast and apply hit effect
        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, range))
        {
            if (hit.transform.gameObject.GetComponent<Target>() != null)
            {
                hit.transform.gameObject.GetComponent<Target>().Touch();
            }
            if (hit.transform.gameObject.CompareTag("Leaf") && secret)
            {
                FindObjectOfType<Secret>().UnlockSecret();
            }
        }
    }
}